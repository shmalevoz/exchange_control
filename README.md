# Центр настроек

## Цель

Иметь одну точку настроек и управления составом обмена для каждого участника и используемого транспорта

## Общее

[API управления](https://elements-demo.stoplight.io/?spec=https://gitlab.com/shmalevoz/exchange_control/-/raw/main/api/services/Exchange-control.json)

[Схема данных](api/datastruct.md)

[Общий процесс](wants/process.md)

[Тесты](tests/priority.md)

[Деплой изменений](wants/changes_deploy_process.md)

## Получение информации об элементах обмена

[Участники обмена](https://elements-demo.stoplight.io/?spec=https://gitlab.com/shmalevoz/exchange_control/-/raw/main/api/services/Exchange-control.json#/operations/members-get) идентифицируются в текущих базах как код на ПланОбмена.ОбменМеждуБазами.ЭтотУзел, для корреспондентов узел обмена <> ЭтотУзел

***Участники*** имеют [Публикации](https://elements-demo.stoplight.io/?spec=https://gitlab.com/shmalevoz/exchange_control/-/raw/main/api/services/Exchange-control.json#/operations/v1-members-memberName-points-get) - это какие-то публикации на серверах, доступные для http запросов

***Публикации*** реализуют доступы к [Сервисам](https://elements-demo.stoplight.io/?spec=https://gitlab.com/shmalevoz/exchange_control/-/raw/main/api/services/Exchange-control.json#/operations/v1-members-memberName-points-pointName-services-get) - это конечные точки обработки данных

***Сервисы*** могут быть как издателями, так и потребителями данных. Издатели это producers, потребители это consumers

***Сервисы*** соединены с [Транспортом](https://elements-demo.stoplight.io/?spec=https://gitlab.com/shmalevoz/exchange_control/-/raw/main/api/services/Exchange-control.json#/operations/v1-transports-get).

* Издатели соединены с каким-то [обменом (Exchange)](https://elements-demo.stoplight.io/?spec=https://gitlab.com/shmalevoz/exchange_control/-/raw/main/api/services/Exchange-control.json#/operations/v1-transports-transportName-exchanges-get)
* Потребители соединены с какой-то [очередью (Queue)](https://elements-demo.stoplight.io/?spec=https://gitlab.com/shmalevoz/exchange_control/-/raw/main/api/services/Exchange-control.json#/operations/v1-transports-transportName-queues-get)

И ***Обмены*** и ***Очереди*** поддерживают конечный состав проходящих через них [потоков данных](https://elements-demo.stoplight.io/?spec=https://gitlab.com/shmalevoz/exchange_control/-/raw/main/api/services/Exchange-control.json#/operations/v1-streams-get)

***Поток данных*** содержит передаваемые по нему объекты и [ссылки на схемы данных](https://elements-demo.stoplight.io/?spec=https://gitlab.com/shmalevoz/exchange_control/-/raw/main/api/services/Exchange-control.json#/operations/v1-schemas-get), используемые для представления этих объектов

***Схемы данных*** могут иметь [версии](https://elements-demo.stoplight.io/?spec=https://gitlab.com/shmalevoz/exchange_control/-/raw/main/api/services/Exchange-control.json#/operations/v1-schemas-schemaName-versions-get) и рассказывают о [реализации объектов](https://elements-demo.stoplight.io/?spec=https://gitlab.com/shmalevoz/exchange_control/-/raw/main/api/services/Exchange-control.json#/operations/v1-schemas-schemaName-versions-versionName-objects-get) в каждой [конкретной версии](https://elements-demo.stoplight.io/?spec=https://gitlab.com/shmalevoz/exchange_control/-/raw/main/api/services/Exchange-control.json#/operations/v1-schemas-schemaName-versions-versionName-objects-get)

То есть для получения набора сообщений по объекту необходимо

* Получить Публикации -> Сервисы -> Точки транспорта -> Потоки данных
* Для каждого потока получить схему данных
* Для каждой уникальной схемы сформировать сообщение
* Для каждой уникальной схемы сформировать ключ адресации по сумме потоков-владельцев схемы
* Сформированные сообщения передать сервису на отправку в связанную точку транспорта

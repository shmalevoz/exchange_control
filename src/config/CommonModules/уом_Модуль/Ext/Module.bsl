﻿// Обращение к модулям других подсистем
//  

#Область ПрограммныйИнтерфейс

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

// Возвращает общий модуль ом_ОбъектПрикладной
//
// Параметры: 
//
// Возвращаемое значение: 
// 	ОбщийМодуль
//
Функция ом_ОбъектПрикладной() Экспорт
	
	Возврат Ссылка("ом_ОбъектПрикладной");
	
КонецФункции // ом_ОбъектПрикладной 

// Возвращает общий модуль ом_Тип
//
// Параметры: 
//
// Возвращаемое значение: 
// 	ОбщийМодуль
//
Функция ом_Тип() Экспорт
	
	Возврат Ссылка("ом_Тип");
	
КонецФункции // ом_Тип 

// Возвращает общий модуль  ом_Значение
//
// Параметры: 
//
// Возвращаемое значение: 
// 	ОбщийМодуль
//
Функция ом_Значение() Экспорт
	
	Возврат Ссылка("ом_Значение");
	
КонецФункции // ом_Значение 

// Возвращает общий модуль ом_Метаданные
//
// Параметры: 
//
// Возвращаемое значение: 
// 	ОбщийМодуль
//
Функция ом_Метаданные() Экспорт
	
	Возврат Ссылка("ом_Метаданные");
	
КонецФункции // ом_Метаданные 

// Возвращает общий модуль оас_Контекст
//
// Параметры: 
//
// Возвращаемое значение: 
// 	ОбщийМодуль
//
Функция оас_Контекст() Экспорт
	
	Возврат Ссылка("оас_Контекст");
	
КонецФункции // оас_Контекст 

// Возвращает общий модуль оас_Ответ
//
// Параметры: 
//
// Возвращаемое значение: 
// 	ОбщийМодуль
//
Функция оас_Ответ() Экспорт
	
	Возврат Ссылка("оас_Ответ");
	
КонецФункции // оас_Ответ 

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// Возвращает ссылку на общий модуль
//
// Параметры: 
// 	Имя - Строка - Имя общего модуля
//
// Возвращаемое значение: 
// 	ОбщийМодуль
//
Функция Ссылка(Имя)
	
	Возврат Вычислить(Имя);
	
КонецФункции // Ссылка 

#КонецОбласти
 